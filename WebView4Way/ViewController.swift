//
//  ViewController.swift
//  WebView4Way
//
//  Created by Jason Stelzel on 6/17/20.
//  Copyright © 2020 Jason Stelzel. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {

    var webView: WKWebView!
    var showingCNN: Bool = false

    override func loadView() {
        super.loadView()
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.scrollView.horizontalScrollIndicatorInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 9000)
        webView.scrollView.verticalScrollIndicatorInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 9000)
        view = webView
        print ("view loaded")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let url = URL(string: "https://cnn.com")!
        let url = URL(string: "https://ujibyqaa8h.execute-api.us-east-1.amazonaws.com/envoiMultiplexVideo")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        print ("View did load")
    }

    override func motionBegan(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        print("Device was shaken!")
        handleTap()
    }
    
    @objc func handleTap() {
        if (showingCNN) {
            showingCNN = !showingCNN
            let url = URL(string: "https://ujibyqaa8h.execute-api.us-east-1.amazonaws.com/envoiMultiplexVideo")!
            webView.load(URLRequest(url: url))
        } else {
            showingCNN = !showingCNN
            let url = URL(string: "https://cnn.com")!
            webView.load(URLRequest(url: url))
        }
    }

}

